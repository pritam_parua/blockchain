const SHA256 = require('crypto-js/sha256');

class Block{
constructor(index, timestamp, data, previousHash = ''){
this.index = index;
this.timestamp = timestamp;
this.data = data;
this.previousHash = previousHash;
this.hash = this.calculateHash();
this.nonce = 0;
}

calculateHash(){
return SHA256(this.indxe + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
}

mineBlock(dificulty){
	while(this.hash.substring(0, dificulty) !== Array(dificulty + 1).join("0")){
	this.nonce++;
	this.hahs = this.calculateHash();
}

	cosole.log("Block mined: " + this.hash);
}
}

class Blockchain{
		constructor(){
				this.chain = [this.createInitialBlock()];
				this.dificulty = 1;
		}

		createInitialBlock(){
				return new Block(0, "01/01/2017", "initial block", "0");
		}

		getLatestBlock(){
			return this.chain[this.chain.length - 1];
		}

		addBlock(newBlock){
			newBlock.previousHash = this.getLatestBlock().hash;
			newBlock.hash=newBlock.calculateHash();
			this.chain.push(newBlock);
		}

		isChainValid(){
		 	for(let i = 1; i < this.chain.length; i++){
				const currentBlock = this.chain[i];
				const previousBlock = this.chain[i - 1];

				if(currentBlock.hash != currentBlock.calculateHash()){
					return false;
				}

				if(currentBlock.previousHash !== previousBlock.hash){
					return false;
				}
			}

			return true;
		}
}

let medicalReport = new Blockchain();

console.log('Mining block 1...');
medicalReport.addBlock(new Block(1,"10/07/2017", { name: 'pritam' }));

console.log('Mining block 2...');
medicalReport.addBlock(new Block(2,"12/07/2017", { name: 'Mr. Dado' }));
console.log(JSON.stringify(medicalReport));